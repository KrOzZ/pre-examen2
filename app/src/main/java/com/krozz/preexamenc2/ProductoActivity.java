package com.krozz.preexamenc2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.krozz.preexamenc2.database.Producto;
import com.krozz.preexamenc2.database.ProductosCon;

public class ProductoActivity extends AppCompatActivity {

    private EditText txtCodigo;
    private Button btnBuscar;
    private EditText txtProducto;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup rgPerecedero;
    private RadioButton rbPRSI;
    private RadioButton rbPRNO;
    private Button btnBorrar;
    private Button btnActualizar;
    private Button btnCerrar;
    private Producto saveObject = null;
    private ProductosCon db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);
        txtCodigo = findViewById(R.id.txtCodigo);
        btnBuscar = findViewById(R.id.btnBuscar);
        txtProducto = findViewById(R.id.txtProducto);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rgPerecedero = findViewById(R.id.rgPerecedero);
        rbPRSI = findViewById(R.id.rbPRSI);
        rbPRNO = findViewById(R.id.rbPRNO);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnCerrar = findViewById(R.id.btnCerrar);
        db = new ProductosCon(this);

        btnActualizar.setOnClickListener(this::btnActualizarAction);
        btnCerrar.setOnClickListener(v -> finish());
        btnBuscar.setOnClickListener(this::btnBuscarAction);
        btnBorrar.setOnClickListener(this::btnBorrarAction);

    }


    private void btnBuscarAction(View view)
    {
        db.openDatabase();
        Producto producto = db.getProducto(Long.parseLong(getText(txtCodigo)));
        if(producto == null)
        {
            Toast.makeText(this, "No se encontro producto", Toast.LENGTH_LONG).show();
        }
        else
        {
            saveObject = producto;
            txtMarca.setText(saveObject.getMarca());
            txtProducto.setText(saveObject.getNombreProducto());
            txtPrecio.setText(String.valueOf(saveObject.getPreacio()));
            rgPerecedero.check(saveObject.isPerecedero() ? R.id.rbPRSI : R.id.rbPRNO);
        }
        db.close();
    }

    private void btnActualizarAction(View view) {
        db.openDatabase();
        if(validar())
        {
            Producto producto = new Producto();
            producto.setId(saveObject.getId());
            producto.setNombreProducto(getText(txtProducto));
            producto.setPreacio(Float.parseFloat(getText(txtPrecio)));
            producto.setMarca(getText(txtMarca));
            producto.setPerecedero(rgPerecedero.getCheckedRadioButtonId() == R.id.rbPRSI);
            if (db.update(producto) != -1)
                Toast.makeText(this, "Se actualizo correctamente", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "Error Al Actualizar", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "Favor de Llenar todos los campos",
                    Toast.LENGTH_SHORT).show();

        }
        limpiar();
        db.close();
    }
    private void btnBorrarAction(View view)
    {
        db.openDatabase();
        db.delete(Long.parseLong(getText(txtCodigo)));
        Toast.makeText(this, "Se elimino el producto", Toast.LENGTH_LONG).show();
        db.close();
        limpiar();
    }

    private void limpiar()
    {
        txtCodigo.setText("");
        txtPrecio.setText("");
        txtProducto.setText("");
        txtMarca.setText("");
        rbPRSI.setChecked(true);
        saveObject = null;

    }
    private boolean validar(){
        if(validarCampo(txtCodigo) || validarCampo(txtProducto) ||
                validarCampo(txtPrecio) || validarCampo(txtMarca))
            return false;
        else
            return true;
    }

    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }
}
