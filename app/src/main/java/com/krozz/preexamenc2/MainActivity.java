package com.krozz.preexamenc2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.krozz.preexamenc2.database.Producto;
import com.krozz.preexamenc2.database.ProductoDBHelper;
import com.krozz.preexamenc2.database.ProductosCon;

public class MainActivity extends AppCompatActivity {

    private EditText txtCodigo;
    private EditText txtProducto;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup rgPerecedero;
    private RadioButton rbPRSI;
    private RadioButton rbPRNO;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnNuevo;
    private Button btnEditar;
    private ProductosCon db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtCodigo = findViewById(R.id.txtCodigo);
        txtProducto = findViewById(R.id.txtProducto);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rgPerecedero = findViewById(R.id.rgPerecedero);
        rbPRSI = findViewById(R.id.rbPRSI);
        rbPRNO = findViewById(R.id.rbPRNO);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnNuevo = findViewById(R.id.btnNuevo);
        btnEditar = findViewById(R.id.btnEditar);
        db = new ProductosCon(this);

        btnGuardar.setOnClickListener(this::btnGuardarAction);
        btnLimpiar.setOnClickListener(this::btnLimpiarAction);
        btnEditar.setOnClickListener(this::btnEditarAction);
        btnNuevo.setOnClickListener(this::btnNuevoAction);

    }
    private void btnNuevoAction(View view) {
        limpiar();
    }

    private void btnEditarAction(View view) {
        Intent intent = new Intent(this, ProductoActivity.class);
        startActivity(intent);

    }

    private void btnLimpiarAction(View view) {
        limpiar();
    }



    private void btnGuardarAction(View view) {
        db.openDatabase();
        if(validar())
        {
            Producto producto = new Producto();
            producto.setCodigo(Integer.parseInt(getText(txtCodigo)));
            producto.setNombreProducto(getText(txtProducto));
            producto.setPreacio(Float.parseFloat(getText(txtPrecio)));
            producto.setMarca(getText(txtMarca));
            producto.setPerecedero(rgPerecedero.getCheckedRadioButtonId() == R.id.rbPRSI);
            Producto producBus = db.getProducto(Long.parseLong(getText(txtCodigo)));
            if(producBus == null)
            {
                if (db.insert(producto) != -1)
                    Toast.makeText(MainActivity.this, "Se agrego correctamente", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(MainActivity.this, "Se produjo un error", Toast.LENGTH_SHORT).show();
            }
            else
                Toast.makeText(MainActivity.this, "ya existe", Toast.LENGTH_SHORT).show();

        }
        else
        {
            Toast.makeText(MainActivity.this, "No puedes dejar los campos vacios",
                    Toast.LENGTH_SHORT).show();

        }
        limpiar();
        db.close();
    }
    private void limpiar()
    {
        txtCodigo.setText("");
        txtPrecio.setText("");
        txtProducto.setText("");
        txtMarca.setText("");
        rbPRSI.setChecked(true);
    }

    private boolean validar(){
        if(validarCampo(txtCodigo) || validarCampo(txtProducto) ||
                validarCampo(txtPrecio) || validarCampo(txtMarca))
            return false;
        else
            return true;
    }

    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt) {
        return txt.getText().toString();
    }
    }
